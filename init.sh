#!/bin/bash
#
# @@script: init.sh
# @@description: run livedev listening server
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

PATH="/path/to/folder"
linux="ubuntu"

### runas root
function runas_root () {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
      then
         echo "[livedev] Permission denied."
         exit 1
   fi
}

### init server
function start_live () {
  # check prerequisite
  if which node >/dev/null;
      then
      nodejs livedev.js ${PATH}
  else
      # install prerequisites
      echo "[livedev] Node.js required. Installing..."

      bash install-node.sh ${linux}
  fi
}

### initialize
start_live
