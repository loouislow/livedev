## liveup

> Ultra thinnest live development toolkit. Use it for hacking your HTML/JavaScript/CSS files.
---
## Installation

You need node.js and npm. Usually when you run `init.sh` will automatically install for you.

> https://nodejs.org/en/download/package-manager/

```bash
$ npm install -g n
$ npm n 6.10.2
```

```bash
$ git clone https://github.com/delacroix81/liveup.git
$ cd liveup
$ npm install .
```

To run,

```bash
$ nodejs nlive.js /var/www/
```

Or customize preset script on your project,

```bash
$ ./init.sh
```
---
## Usage

The `liveup` will automatically launch the default browser. When you make a change to any file, the browser will reload the page - unless it was a CSS file in which case the changes are applied without a reload.

Command line parameters:

* `--port=NUMBER` - select port to use, default: PORT env var or 8080
* `--host=ADDRESS` - select host address to bind to, default: IP env var or 0.0.0.0 ("any address")
* `--no-browser` - suppress automatic web browser launching
* `--browser=BROWSER` - specify browser to use instead of system default
* `--quiet | -q` - suppress logging
* `--verbose | -V` - more logging (logs all requests, shows all listening IPv4 interfaces, etc.)
* `--open=PATH` - launch browser to PATH instead of server root
* `--watch=PATH` - comma-separated string of paths to exclusively watch for changes (default: watch everything)
* `--ignore=PATH` - comma-separated string of paths to ignore
* `--ignorePattern=RGXP` - Regular expression of files to ignore (ie `.*\.jade`) (**DEPRECATED** in favor of `--ignore`)
* `--middleware=PATH` - path to .js file exporting a middleware function to add; can be a name without path nor extension to reference bundled middlewares in `middleware` folder
* `--entry-file=PATH` - serve this file (server root relative) in place of missing files (useful for single page apps)
* `--mount=ROUTE:PATH` - serve the paths contents under the defined route (multiple definitions possible)
* `--spa` - translate requests from /abc to /#/abc (handy for Single Page Apps)
* `--wait=MILLISECONDS` - (default 100ms) wait for all changes, before reloading
* `--htpasswd=PATH` - Enables http-auth expecting htpasswd file located at PATH
* `--cors` - Enables CORS for any origin (reflects request origin, requests with credentials are supported)
* `--https=PATH` - PATH to a HTTPS configuration module
* `--proxy=ROUTE:URL` - proxy all requests for ROUTE to URL
* `--help | -h` - display terse usage hint and exit
* `--version | -v` - display version and exit
