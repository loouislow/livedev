#!/bin/bash
#
# @@script: install-node.sh
# @@description: Quick install Node.js on debian/ubuntu/arch or redhat/fedora
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

### runas root
function runas_root () {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
      then
         echo "[nano] Permission denied."
         exit 1
   fi
}

# No args
if [ $# -eq 0 ]
  then
    echo "Which one? : [debian|redhat]"
    exit 1
fi

# Too many args
if [ $# -ge 2 ]
  then
    echo "Which one? : [debian|redhat]]"
    exit 1
fi

# Debian/Ubuntu/Arch
if [ "$1" == "debian" ]
then
	curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
	sudo apt-get install -y nodejs
fi

# Redhat/Fedora
if [ "$1" == "redhat" ]
then
  curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
  yum -y install nodejs
fi
 
